; $Id$
Restricts access to content-types by role. The code of this module is heavily borrowed from module �Premium� v6.x-1.x-dev by Allie Micka, on which this module is based. New features are restricting content by content-types rather than per node and restricting to more than one user-role.

From Premium (but still applicable to this module as well):
This is useful on a news or membership site where teasers are available to the general public but the full body is only available to privileged users. Restricted content appears in listings with full title and teaser available to anyone. If a user does not have adequate privileges, the body is replaced with an administrator-defined message (for example, an invitation to join the site).

Restricted Content Types features time settings, so restricted status can end (protecting latest content), or begin (protecting archived content) after an admin-defined timeframe.

Because this module does not use the node access system, it does not hide items that are unavailable to a user and is more efficient for large sites. Restricted Content Types can be used in conjunction with any node access module to provide advanced access control.